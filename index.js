const mongoose = require("mongoose");
const Schema = mongoose.Schema

var personSchema = new Schema({
    name: { type: String },
    age: { type: Number},
      phone: { type: Number},

    });

var personModel = mongoose.model('Person', personSchema);
module.exports = personModel;