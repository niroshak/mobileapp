var express = require("express");
var bodyParser = require("body-parser");
// var routes =  require('./Routes/routes');
var mongoose = require("mongoose");
var info = require("./index")
var app = express();
app.use(bodyParser.json());
// app.use('/api', routes);
mongoose.connect('mongodb://localhost:27017/user',{ useNewUrlParser: true }).then(() => {
  console.log('Successfully connected to the database');
})
.catch(err => {
  console.log('Could not connect to the database.', err);
  process.exit();
});

var userdata=[
{
  id: Number,
  message: String
}]

 //GET ALL USERS DATA
 app.get("/users", function(req, res){
   console.log(req, "req")
   info.find({}).then((data) => {
    console.log("data", data) 
    res.send(data);
  })
 })

//GET USER BY ID
app.get('/users/:id', function(req, res) {
 info.findById(req.params.id).then(function(ninja) {
  res.send(ninja)})
});


//CREATE A NEW USER
app.post('/users/add-data', function(req, res){
  console.log("WOrking like we think", req.body)
  info.create(req.body).then(function(ninja){
   res.send(ninja)})
 // }).then(function(ninja){
 //   console.log(ninja, "ninjas")
 // })
});

//UPDATE A USER DATA
app.put('/users/updatedata/:id', function(req, res){
 info.findByIdAndUpdate({_id:req.params.id}, req.body).then(()=> {
   info.findOne({id:req.params.id}).then((ninja)=>{
     res.send(ninja)
   })
 })
})

//DELETE A USER 
app.delete('/users/deletedata/:id', function(req, res){
 info.findByIdAndRemove({_id:req.params.id}).then(function(ninja){
   res.send({ninja})
 })
 console.log("delete method, value deleted")
})

//LISTEN TO SERVER
const port = 5001;
app.listen(port, function(){
 console.log("server is running on port No:", port)
})